# filters:
#### the filter parameter, takes one or more object.
##### filter can be any 'Field' from:
https://core.telegram.org/bots/api#update  

message  
edited_message  
channel_post   
edited_channel_post   
inline_query  
chosen_inline_result  
callback_query  
shipping_query  
pre_checkout_query  
poll  

'Field' from:  
https://core.telegram.org/bots/api#message 
from  
date  
chat  
forward_from  
forward_from_chat  
forward_from_message_id  
forward_signature  
forward_sender_name  
forward_date  
reply_to_message  
edit_date  
media_group_id  
author_signature  
text  
entities  
caption_entities  
audio  
document  
animation  
game  
photo  
sticker  
video  
voice  
video_note  
caption  
location  
venue  
poll  
new_chat_members  
left_chat_member  
new_chat_title  
new_chat_photo  
delete_chat_photo  
group_chat_created  
supergroup_chat_created  
channel_chat_created  
migrate_to_chat_id  
migrate_from_chat_id  
pinned_message  
invoice  
successful_payment  
connected_website  
passport_data  

'Field' from:
https://core.telegram.org/bots/api#inlinequery 

location  
query  
offset  

'Field' from:
https://core.telegram.org/bots/api#choseninlineresult

result_id  
inline_message_id  

'Field' from:
https://core.telegram.org/bots/api#callbackquery

chat_instance  
data  
game_short_name  

'Field' from:
https://core.telegram.org/bots/api#shippingquery 
and  
https://core.telegram.org/bots/api#precheckoutquery

currency  
total_amount  
invoice_payload  
shipping_option_id  
order_info  

and 'Field' from:
https://core.telegram.org/bots/api#poll

id  
question  
options  
is_closed  

'Type' from:
https://core.telegram.org/bots/api#messageentity

mention (@username)  
hashtag  
cashtag  
bot_command  
url  
email  
phone_number  
bold (bold text)  
italic (italic text)  
code (monowidth string)  
pre (monowidth block)  
text_link (for clickable text URLs)  
text_mention (for users without usernames)  
.  

#### filter on text or photo. The function below responds each time an update contains text or an image.
``` python
@bot(filters=('text', 'photo'))
def on_text_or_photo(update):
    update.reply('text or photo')
```  

# not_on:
#### just like 'filters' parameter but negative.

##### The function below responds to any update except those that contain 'Document'.
``` python
@bot(not_on='document')
def not_on_document(update):
    update.reply("it's not Document")
```  

# chat_id:
#### id of chat

##### The function below responds to any update from specified chat_id
``` python
@bot(chat_id=123456789)
def example_chat_id(update):
    update.reply('message from chat_id123456789')
```  


# chat_type:
#### Type of chat, can be either “private”, “group”, “supergroup” or    “channel”

##### The function below responds to any update from 'supergroup' chat
``` python
 @bot(chat_type='supergroup')
def msg_from_supergroup(update):
    update.reply('this is a supergroup')
```  

# from_id
#### id of user or bot
##### The function below responds to any update from specified user id
```
@bot(from_id=123456789)
def example_from_id(update):
    update.reply('message from user_id 123456789')
```  

# regex
#### regular expression pattern
##### The function below responds to updates contains Arab characters in the text or caption
``` python
@bot(regex='[🇮🇷ء-ي٠-٩]')
def example_regex(update):
    update.reply('hi you')
```  

# callback_query
#### regular expression pattern
##### The function below responds to callback_query contains regex pattern in `update.data`
``` python
@bot(callback_query='re-pattren')
def example_callback_query(update):
    update.reply('hi you')
```  

# inline_query
#### regular expression pattern
##### The function below responds to inline_query contains regex pattern in `update.query`
``` python
@bot(inline_query='re-pattren')
def example_inline_query(update):
    update.reply('hi you')
```  


# command
#### bot_command
##### The function below responds to updates start with '/start' or '/help'.
``` pytohn
@bot(command=('start', 'help'))
def start_and_help(update):
    update.reply('Welcome!')
```  

# create
#### Custom Filter
##### pass a function for filtering if the function return True, the function below responds to updates
example using `lambda`:

``` pytohn
@bot(create=lambda up: 'text' in up)
def reply_to_text(update):
    update.reply('hi')
```  

example 2 using a function:
``` python

```

# on_error
#### all errors
##### The function below responds to any errors from the requests the functions.
``` python
@bot(on_error=True)
def errors(update):
    print('Error: %s' % update)
```
