from Tele import *
from uuid import uuid4


""" make sure to enable inline mode with @BotFather
 via /setinline command """


@bot('inline_query')
def inline(update):
    if update.query:
        # list of results
        results = [

            # Monospace text in Markdown parse_mode
            InlineQueryResultArticle(
                id=str(uuid4()),
                title="Monospace",
                input_message_content=InputTextMessageContent(
                    "`{}`".format(update.query),
                    parse_mode='Markdown')),

            # Italic text in HTML parse_mode
            InlineQueryResultArticle(
                id=str(uuid4()),
                title="Italic",
                input_message_content=InputTextMessageContent(
                    "<i>{}</i>".format(update.query),
                    parse_mode='HTML'))]

        answerInlineQuery(update, results)


account('TOKEN')


bot_run(multi=True)

