from Tele import *

# json Module, already import via Tele. 

# this function respond to any messages except 'inline_query'
@bot(not_on='inline_query')
def all_msg(up):
    message_reply(up, '`%s`' % json.dumps(up,
                                          indent=4,
                                          ensure_ascii=False),
                  parse_mode='Markdown')


# this function respond only to 'inline_query' messages
@bot('inline_query')
def inline(up):
    sendMessage(up['from'].id,
                text='`%s`' % json.dumps(up,
                                         indent=4,
                                         ensure_ascii=False),
                parse_mode='Markdown')


account('TOKEN')
bot_run(offset_=True, multi=True)

