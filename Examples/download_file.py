from Tele import *


# download_file(file_id, name, destination='')

# To download the most high quality image from the 'PhotoSize'
@bot('photo')
def photo(update):
    download_file(update.photo[-1].file_id, name='photo.jpg')


@bot('audio')
def audio(update):
    download_file(update.audio.file_id, name=update.audio.title)


@bot('document')
def document(update):
    download_file(update.document.file_id, name=update.document.file_name)


@bot('video')
def video(update):
    download_file(update.video.file_id, name='new_video.mp4')


@bot('animation')
def animation(update):
    download_file(update.animation.file_id, name=update.animation.file_name)


@bot('voice')
def voice(update):
    download_file(update.file_id, name='new_voice')


@bot('video_note')
def video_note(update):
    download_file(update.video_note.file_id, name='new_video_note')


account('TOKEN')
bot_run()
