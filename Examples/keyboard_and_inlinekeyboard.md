
# using the Keyboard and Inlinekeyboard function

##### a regular keyboard with 3 elements in each line.
``` python
reply_markup = Keyboard([[1, 2, 3],
                         [4, 5, 6],
                         [7, 8, 9],
                         [10]])
```
### or, you can use the automatic placement by using **num_line** :
``` pytohn
reply_markup = Keyboard([1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                         num_line=3)
```
.
#### a regular Inline keyboard with 3 elements in each line
``` python
reply_markup = InlineKeyboard([[{'1': '1'}, {'2': '2'}, {'3': '3'}],
                                [{'4': '4'}, {'5': '5'}, {'6': '6'}],
                                [{'7': '7'}, {'8': '8'}, {'9': '9'}],
                                [{'10': '10'}]])
```

##### and same here, you can use the **num_line** to ordering:
``` python
reply_markup = InlineKeyboard([{'1': '1'}, {'2': '2'}, {'3': '3'}, {'4': '4'}, {'5': '5'},
                                {'6': '6'}, {'7': '7'}, {'8': '8'}, {'9': '9'}, {'10': '10'}],
				 num_line=3)
```
.
## using different types of inline keyboard.
#### The default type is "callback_data". so if you don't Specifies type it will be callback_data.

### sending inline keyboard with different types of buttons:
``` python
reply_markup = InlineKeyboard([{'text': 'callback_data'},
                                {'google': 'https://www.google.com', 'type': 'url'},
                                {'text2': 'bla..'}, {'youtube': 'https://www.youtube.com', 'type': 'url'}],
			         num_line=2)
```
.
#### Alternatively, if most buttons url [or other..] type, you can change the default type by using 'obj='<type>'.
##### In that case, if you need to sen a 'callback_data' button you need to add 'type': 'callback_data'.
``` python
reply_markup = InlineKeyboard([{'text': 'bla', 'type': 'callback_data'},
                                {'google': 'https://www.google.com'},
                                {'youtube': 'https://www.youtube.com'}],
				 num_line=2,
				 obj='url')
```

# sending the message..
``` python
sendMessage(my_id, 'some text', reply_markup=reply_markup)
```

