from Tele import *


# This function will respond to messages which contain /start command
@bot(command='start')
def cmd(update):
    text = """
    hi {}!! 🖐
    my name is mr Bot.
    please tell me about yourself. are you male or female?
    you can use /cancel command to stop this chat.
    """.format(update['from']['first_name'])

    reply_markup = Keyboard([['Male', 'Female'], ['other']])
    update.reply(
        text=text,
        reply_markup=reply_markup
    )


# This function will respond to messages which contain /cancel command
@bot(command='cancel')
def cancel(update):
    update.reply(
        'Okay, I will not disturb you. 🙏',
        reply_markup=Keyboard(remove=True)
    )


# This function will respond to text messages which contain
# 'Male', 'Female', or 'other'
@bot(regex='^(Male|Female|other)$')
def gender(update):
    # remove Keyboard
    update.reply(
        text='Good for you!',
        reply_markup=Keyboard(remove=True)
    )

    sendMessage(
        chat_id=update['from']['id'],
        text='how old are you?🤔',
        reply_markup=InlineKeyboard(
            [[{'0-10': '10'}, {'10-15': '15'}],
             [{'15-20': '20'}, {'20+': '21'}]])
    )


# This function will respond to messages which contain data from 'callback_data'
@bot('callback_query')
def on_markup(update):
    age = int(update.data)
    if age < 10:
        update.reply("You're very young😌")
    elif age < 15:
        update.reply("you are young😌")
    elif age < 21:
        update.reply("You're an excellent age")
    else:
        update.reply(
            "You have to do with your life something"
            " instead of chatting with bots ..")


def main(token):
    account(token)
    bot_run(offset_=True)


if __name__ == '__main__':
    TOKEN = 'Your token'
    main(TOKEN)


