.. tele documentation master file, created by
   sphinx-quickstart on Mon Mar 18 12:28:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

view on `GitLab <https://gitlab.com/2411eliko/tele>`_

Welcome to Tele's documentation!
================================
**Tele** is a library for building `Telegram <https://core.telegram.org/bots/api/>`_  bots in a simple and fast way.

for example.
Send a message with text 'hi' and a keyboard with 10 buttons, 3 in each line::
    from Tele import *
    sendMessage(12345678, 'hi',
            reply_markup=Keyboard([r for r in range(10)], num_line=3))



Indices and tables
==================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
    code

* :ref:`search`


`Module index <tele.readthedocs.io/en/latest/code.html>`_


Getting Started
^^^^^^^^^^^^^^^


Installing
----------

.. code:: shell

    pip3 install telepy


starting the code with `import Tele` [or `form Tele import` ..]
and insert token to "accuont" function.
::
    import Tele


    Tele.account('<Your Token Here>')
    Tele.sendMessage(chat_id=12345678, text='hello')



To receive information about sent messages, and to respond to them
Use the `@bot()` decorator,  above youre functions.
The functions will respond every time a message is sent.
and the argument will be the update from 'getUpdates' [json].
To start the functions add `bot_run()` after the `account()`
::
    from Tele import *
    

    @bot()
    def hello_world(update):
        print(update)    
    
    
    account('<Your Token Here>')
    bot_run()



In this case, the function (hello_world) will run each time a message is sent to the bot.


the @bot decorator, can take filters and respond to particular type of messages, According to what you have determined.
::
    @bot('text')
        def hello_world(update):
	    print(update)


    @bot(('photo', 'bot_command'))
        def func(up):
            print(up.['from'].id)

see `here <https://gitlab.com/2411eliko/tele/blob/master/Examples/filters.md>`_ information about the filters 


