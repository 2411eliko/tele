#### Tele
Telegram bot library.  

view [readthedocs](https://tele.readthedocs.io)  
[GitLab](https://gitlab.com/2411eliko/tele)  
[pypi](https://pypi.org/project/telepy/)  
![Build Status](https://img.shields.io/pypi/dm/telepy)  
##### Installation

requires python 3.4 or higher.

`pip3 install telepy`

```python 
from Tele import *


@bot('text')
def echo(update):
    update.reply(update.text)


account('TOKEN')
bot_run()

```
